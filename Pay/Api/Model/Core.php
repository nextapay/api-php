<?php

namespace Pay\Api\Model;

/**
 * Model base class, provide some important functions to payment class
 *
 * @package    PaymentAPI
 * @version    1.0
 */
abstract class Core
{
    protected $required = [];

    /**
     * Core constructor.
     *
     * @param array $data
     *
     * @throws \Exception
     */
    function __construct($data = [])
    {
        $params = array_keys(get_object_vars($this));

        foreach ($data as $attr => $value) {
            if (in_array($attr, $params)) {
                $this->$attr = $value;
            } else {
                throw new \Exception("Invalid Parameter: {$attr}");
            }
        }
    }

    /**
     * Return class name, used as json attr name
     *
     * @param bool $basename
     *
     * @return string
     */
    public function getName($basename = false)
    {
        $name = get_called_class();

        if ($basename) {
            $name = strtolower(basename(str_replace('\\', '/', $name)));
        }

        return $name;
    }

    /**
     * Transform object into array attributes
     *
     * @param array $append
     *
     * @return array
     */
    public function getData($append = [])
    {
        $vars = get_object_vars($this);

        /** remove attribute required to avoid sending via api */
        unset($vars['required']);

        if (is_array($append) && !empty($append)) {
            $vars = array_merge($vars, $append);
        }

        return $vars;
    }

    /**
     * Verify if required field has valid values
     *
     * @return array
     */
    public function checkRequiredFields()
    {
        $invalid = [];
        foreach ($this->required as $field) {
            if (!$this->$field) {
                $invalid[] = $field;
            }
        }

        if (!empty($invalid)) {
            return ['Class' . $this->getName() . ': ' . implode(', ', $invalid)];
        }

        return [];
    }
}