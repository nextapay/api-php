<?php

namespace Pay\Api\Model;

/**
 * Invoice Model class, provide invoice pattern and required fields
 *
 * @package    PaymentAPI
 * @version    1.0
 */
class Invoice extends Core
{
    /** @var string */
    protected $login;

    /** @var float */
    protected $amount;

    /** @var string */
    protected $currency;

    /** @var string */
    protected $invoice;

    /** @var string */
    protected $description;

    protected $required = ['login', 'amount', 'currency', 'invoice', 'description'];

}