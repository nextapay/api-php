<?php

namespace Pay\Api\Model;

/**
 * Boleto Model, provide boleto pattern and required fields
 *
 * @package    PaymentAPI
 * @version    1.0
 */
class Boleto extends Core
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $email;

    /** @var string */
    protected $cpfCnpj;

    /** @var string */
    protected $address;

    /** @var string */
    protected $addressNumber;

    /** @var string */
    protected $addressComplement;

    /** @var string */
    protected $neighborhood;

    /** @var string */
    protected $city;

    /** @var string */
    protected $stateInitials;

    /** @var string */
    protected $zipCode;

    protected $required = [
        'name',
        'email',
        'cpfCnpj',
        'address',
        'addressNumber',
        'neighborhood',
        'city',
        'stateInitials',
        'zipCode',
    ];

}