<?php

namespace Pay\Api;

use Pay\Api\Model\Core;
use Pay\Api\Model\Invoice;

/**
 * This Payment API allow you to access our payment API: send payment requests, verify payment status and receive payment confirmations
 *
 * @package    PaymentAPI
 * @version    1.0
 */
class Payment
{
    const STATUS_WAITING_PAYMENT   = 0;
    const STATUS_PAYMENT_CONFIRMED = 1;
    const STATUS_PAYMENT_CANCELED  = 9;

    /** @var Invoice */
    private $required;

    /** @var Core */
    private $optional;

    /**
     * Verify given data and require provider payment code
     *
     * @param \Pay\Api\Model\Invoice   $required
     * @param \Pay\Api\Model\Core|null $optional
     * @param string                   $paymentMethod
     *
     * @return array
     */
    public function getPaymentCode(Invoice $required, Core $optional = null, $paymentMethod = '')
    {
        $this->required = $required;
        $this->optional = $optional;

        $invalid = $this->required->checkRequiredFields();
        if ($this->optional) {
            $invalid = array_merge($invalid, $this->optional->checkRequiredFields());
        }

        if ($invalid) {
            return [
                'status'  => 'error',
                'message' => 'You need to set all required fields',
                'fields'  => $invalid,
            ];
        }

        $data = $this->required->getData();

        if ($this->optional) {
            $data[$this->optional->getName(true)] = $this->optional->getData();
        }

        return $this->send("pay/{$paymentMethod}", $data);
    }

    /**
     * Get Payment URL by a given payment code
     *
     * @param $payment_code
     *
     * @return string
     */
    public function getPaymentURL($payment_code)
    {
        return rtrim(PAYMENT_PROVIDER_URL, '/')
            . '/payment/view/'
            . $this->signature($payment_code)
            . '/' . $payment_code;
    }

    /**
     * Parse notifications received from merchant.
     * This is a very important class and should be called always when merchant send notifications,
     * it will parse the notification, call a give callback and response merchant on the right way.
     *
     * @param \Closure $onSuccess
     */
    public function notificationCheck(\Closure $onSuccess)
    {
        try {
            $signature = $_SERVER['HTTP_SIGNATURE'];
            $rawJson   = file_get_contents("php://input");
            $invoice   = json_decode($rawJson);

            if ($signature !== $this->signature($rawJson)) {
                throw new \Exception("Signature mismatch", 401);
            }

            ob_start();
            $onSuccess($invoice);
            ob_clean();

            /**
             * confirm to provider you received information
             */
            header('Content-type: application/json');
            echo json_encode([
                'status'  => 'success',
                'message' => 'information received successfully',
            ]);
        } catch (\Exception $e) {
            header('Content-type: application/json');
            echo json_encode([
                'status'  => 'error',
                'message' => $e->getMessage(),
            ], $e->getCode() ?: 500);
        } finally {
            exit;
        }
    }

    /**
     * Verify payment status on merchant
     *
     * @param $paymendCode
     *
     * @return mixed
     */
    public function verifyPayment($paymendCode)
    {
        return $this->send('payment/status', [
            'payment_code' => $paymendCode,
        ]);
    }

    /**
     * Send curl request
     *
     * @param $method
     * @param $data
     *
     * @return mixed
     */
    protected function send($method, $data)
    {
        $data['date']  = date('Y-m-d H:i:s');
        $data['login'] = PAYMENT_PROVIDER_LOGIN;
        $body          = json_encode($data);

        $signature = $this->signature($body);

        $curl = curl_init(rtrim(PAYMENT_PROVIDER_URL, '/') . "/api/{$method}");
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => $body,
            CURLOPT_HTTPHEADER     => [
                "cache-control: no-cache",
                "signature: {$signature}",
            ],
        ]);

        $res  = curl_exec($curl);
        $err  = curl_error($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        header("HTTP/1.0 {$code}");

        return json_decode($err ?: $res);
    }

    /**
     * sign given string with private key
     *
     * @param $string
     *
     * @return string
     */
    private function signature($string)
    {
        return strtoupper(hash_hmac('sha256', pack('A*', $string), pack('A*', PAYMENT_PROVIDER_SECRET)));
    }

}
