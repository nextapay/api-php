<?php

/**
 * Sample of notification endpoint, please, don't print/return anything inside closure, it'll be ignored
 *
 * @package    PaymentAPI
 * @version    1.0
 */

require_once '_setup.php';

$payment = new \Pay\Api\Payment();

$payment->notificationCheck(function ($invoice) {
    /**
     * when information is valid
     */
    if ($invoice->status_code == \Pay\Api\Payment::STATUS_PAYMENT_CONFIRMED) {
        /**
         * $invoice = {
         *        status: "Payment Confirmed",
         *        status_code: "1",
         *        invoice: "YOUR_INVOICE_ID",
         *        confirmed_at: "CONFIRMATION_DATE",
         *        transaction: {
         *            amount: YOUR_INVOICE_AMOUNT,
         *            deposit_date: SCHEDULED_DEPOSIT_DATE
         *      }
         *  }
         */

        /**
         * this payment was confirmed!
         *
         * don't forget to verify if product of this invoice was not delivered yet
         * if not, perform here necessary actions to deliver this invoice products
         */

        //if (PRODUCT_WAS_NOT_DELIVERED_YET)
        //{
        //    //confirm payment end ship product
        //}
    } else if ($invoice->status_code == \Pay\Api\Payment::STATUS_PAYMENT_CANCELED) {
        /**
         * $invoice = {
         *        status: "Payment Canceled",
         *        status_code: "9",
         *        invoice: "YOUR_INVOICE_ID",
         *  }
         */

        /**
         * perform action after payment is canceled
         */

        //...
    }
});