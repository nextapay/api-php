<?php

/**
 * Sample of sending payment requests,
 *  provide invoice data according to your order/invoice registry and boleto data according to your customer
 *
 * @package    PaymentAPI
 * @version    1.0
 */

require_once '_setup.php';

try {
    $required = new \Pay\Api\Model\Invoice([
        'login'       => PAYMENT_PROVIDER_LOGIN,
        'amount'      => '',
        'currency'    => 'BRL',
        'invoice'     => '',
        'description' => '',
    ]);

    $boleto = new \Pay\Api\Model\Boleto([
        'name'              => '',
        'email'             => '',
        'cpfCnpj'           => '',
        'address'           => '',
        'addressNumber'     => '',
        'addressComplement' => '',
        'neighborhood'      => '',
        'city'              => '',
        'stateInitials'     => '',
        'zipCode'           => '',
    ]);

    $payment  = new \Pay\Api\Payment();
    $response = $payment->getPaymentCode($required, $boleto, PAYMENT_PROVIDER_MERCHANT);

    header('Content-type: application/json');
    echo json_encode($response);
} catch (Exception $e) {
    /**
     * user friendly error message
     */
    die($e->getMessage());
}
