<?php

/**
 * Sample of payment setup
 * IMPORTANT: request defined values to payment provider
 *
 * @package    PaymentAPI
 * @version    1.0
 */

include_once 'Pay/Api/Payment.php';
include_once 'Pay/Api/Model/Core.php';
include_once 'Pay/Api/Model/Invoice.php';
include_once 'Pay/Api/Model/Boleto.php';

$provider = 'boleto'; //for
//$provider = 'testbank'; //use this one to try invoice confirmation

if (!defined('PAYMENT_PROVIDER_LOGIN'))    define('PAYMENT_PROVIDER_LOGIN',    '');
if (!defined('PAYMENT_PROVIDER_SECRET'))   define('PAYMENT_PROVIDER_SECRET',   '');
if (!defined('PAYMENT_PROVIDER_URL'))      define('PAYMENT_PROVIDER_URL',      '');
if (!defined('PAYMENT_PROVIDER_MERCHANT')) define('PAYMENT_PROVIDER_MERCHANT', $provider);